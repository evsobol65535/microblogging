from pydantic import BaseModel


class CommentBase(BaseModel):
    text: str


class Comment(CommentBase):
    id: int

    class Config:
        orm_mode = True


class CommentCreate(CommentBase):
    post_id: int


class CommentUpdate(CommentBase):
    pass

