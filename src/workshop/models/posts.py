from pydantic import BaseModel

from datetime import date
from typing import Optional


class PostsBase(BaseModel):
    header: Optional[str]
    text: Optional[str]


class Posts(PostsBase):
    id: int

    class Config:
        orm_mode = True


class PostsCreate(PostsBase):
    pass


class PostsUpdate(PostsBase):
    pass

