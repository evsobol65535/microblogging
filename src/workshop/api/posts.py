from typing import List

from fastapi import (
    APIRouter,
    Depends,
    Response,
    status,
)

from ..models.auth import User
from ..models.posts import (
    Posts,
    PostsCreate,
    PostsUpdate,
)
from ..services.auth import get_current_user
from ..services.posts import PostsService

router = APIRouter(
    prefix='/posts',
)


@router.get('/my', response_model=List[Posts])
def get_posts_my(
            user: User = Depends(get_current_user),
            service: PostsService = Depends(),
):
    return service.get_list_my(user.id)


@router.get('/all', response_model=List[Posts])
def get_posts_all(
            service: PostsService = Depends(),
):
    return service.get_list_all()


@router.get('/user/{user_id}', response_model=List[Posts])
def get_posts_user(
            user_id: int,
            service: PostsService = Depends(),
):
    return service.get_list_my(user_id=user_id)


@router.post('/', response_model=Posts)
def create_posts(
        posts_data: PostsCreate,
        user: User = Depends(get_current_user),
        service: PostsService = Depends(),
):
    return service.create(user_id=user.id, posts_data=posts_data)


@router.get('/{post_id}', response_model=Posts)
def get_post(
        post_id: int,
        user: User = Depends(get_current_user),
        service: PostsService = Depends(),
):
    return service.get_post_id(user_id=user.id, post_id=post_id)


@router.put('/{post_id}', response_model=Posts)
def update_post(
        post_id: int,
        post_data: PostsUpdate,
        user: User = Depends(get_current_user),
        service: PostsService = Depends(),
):
    return service.update_posts(
        user_id=user.id,
        post_id=post_id,
        post_data=post_data,
    )


@router.delete('/{post_id}')
def delete_post(
        post_id: int,
        user: User = Depends(get_current_user),
        service: PostsService = Depends(),
):
    service.delete_post(user_id=user.id, post_id=post_id)
    return Response(status_code=status.HTTP_204_NO_CONTENT)
