from typing import List

from fastapi import (
    APIRouter,
    Depends,
    Response,
    status,
)

from ..models.auth import User
from ..models.comment import (
    Comment,
    CommentCreate,
    CommentUpdate,
)
from ..services.auth import get_current_user
from ..services.comment import CommentService

router = APIRouter(
    prefix='/comments',
)


@router.get('/{post_id}', response_model=List[Comment])
def get_post_comments(
            post_id: int,
            service: CommentService = Depends(),
):
    return service.get_post_comments(post_id)


@router.post('/', response_model=Comment)
def create(
        comments_data: CommentCreate,
        user: User = Depends(get_current_user),
        service: CommentService = Depends(),
):
    return service.create(user_id=user.id, comment_data=comments_data)


@router.put('/{comment_id}', response_model=Comment)
def update(
        comment_id: int,
        comment_data: CommentUpdate,
        user: User = Depends(get_current_user),
        service: CommentService = Depends(),
):
    return service.update(
        user_id=user.id,
        comment_id=comment_id,
        comment_data=comment_data,
    )


@router.delete('/{comment_id}')
def delete(
        comment_id: int,
        user: User = Depends(get_current_user),
        service: CommentService = Depends(),
):
    service.delete(user_id=user.id, comment_id=comment_id)
    return Response(status_code=status.HTTP_204_NO_CONTENT)
