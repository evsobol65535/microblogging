from typing import List
from datetime import datetime

from fastapi import (
    Depends,
    HTTPException,
    status,
)

from sqlalchemy.orm import Session

from .. import tables
from ..database import get_session
from ..models.comment import (
    CommentCreate,
    CommentUpdate,
)


class CommentService:
    def __init__(self, session: Session = Depends(get_session)):
        self.session = session

    def _get_comment_id(self, user_id: int, comment_id: int) -> tables.Comments:
        comment = (
            self.session
            .query(tables.Comments)
            .filter_by(
                id=comment_id,
                user_id=user_id,
            )
            .first()
        )
        if not comment:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)
        return comment

    def get_post_comments(self, post_id: int,) -> List[tables.Comments]:
        query = (
            self.session
            .query(tables.Comments)
            .filter_by(post_id=post_id)
        )
        post_comments = query.all()
        return post_comments

    def create(self, user_id: int, comment_data: CommentCreate) -> tables.Comments:
        comment = tables.Comments(
            **comment_data.dict(),
            user_id=user_id,
            date=str(datetime.now()),
        )
        self.session.add(comment)
        self.session.commit()
        return comment

    def update(self, user_id: int, comment_id: int, comment_data: CommentUpdate) -> tables.Comments:
        comment = self._get_comment_id(user_id, comment_id)
        for field, value in comment_data:
            setattr(comment, field, value)
        self.session.commit()
        return comment

    def delete(self, user_id: int, comment_id: int):
        comment = self._get_comment_id(user_id, comment_id)
        self.session.delete(comment)
        self.session.commit()
