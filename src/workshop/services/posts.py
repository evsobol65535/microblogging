from typing import List
from datetime import datetime

from fastapi import (
    Depends,
    HTTPException,
    status,
)

from sqlalchemy.orm import Session

from .. import tables
from ..database import get_session
from ..models.posts import (
    PostsCreate,
    PostsUpdate,
)


class PostsService:
    def __init__(self, session: Session = Depends(get_session)):
        self.session = session

    def _get_post_id(self, user_id: int, post_id: int) -> tables.Posts:
        post = (
            self.session
            .query(tables.Posts)
            .filter_by(
                id=post_id,
                user_id=user_id,
            )
            .first()
        )
        if not post:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)
        return post

    def get_list_my(self, user_id: int,) -> List[tables.Posts]:
        query = (
            self.session
            .query(tables.Posts)
            .filter_by(user_id=user_id)
        )
        posts = query.all()
        return posts

    def get_list_all(self) -> List[tables.Posts]:
        query = (
            self.session
            .query(tables.Posts)
        )
        posts = query.all()
        return posts

    def create(self, user_id: int, posts_data: PostsCreate) -> tables.Posts:
        posts = tables.Posts(
            **posts_data.dict(),
            user_id=user_id,
            date=str(datetime.now()),
        )
        self.session.add(posts)
        self.session.commit()
        return posts

    def get_post_id(self, user_id: int, post_id: int) -> tables.Posts:
        return self._get_post_id(user_id, post_id)

    def update_posts(self, user_id: int, post_id: int, post_data: PostsUpdate) -> tables.Posts:
        post = self._get_post_id(user_id, post_id)
        for field, value in post_data:
            setattr(post, field, value)
        self.session.commit()
        return post

    def delete_post(self, user_id: int, post_id: int):
        post = self._get_post_id(user_id, post_id)
        self.session.delete(post)
        self.session.commit()




