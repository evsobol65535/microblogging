**Cоздание миграций(для запуска не требуется)**
- alembic revision --autogenerate -m "<Комментарий>"
- alembic upgrade head


**Запуск приложения**
- docker-compose build
- docker-compose up


**Документация к API**
- 127.0.0.1/docs

**PgAdmin**
- 127.0.0.1/5050

**Данные для входа по улолчанию:**
- Логин: admin@admin.org
- Пароль: admin

